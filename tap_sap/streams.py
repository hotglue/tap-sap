"""Stream type classes for tap-sap."""

from pathlib import Path
from typing import Any, Dict, Optional, Union, List, Iterable

from singer_sdk import typing as th  # JSON Schema typing helpers

from tap_sap.client import SapStream

# TODO: Delete this is if not using json files for schema definition
SCHEMAS_DIR = Path(__file__).parent / Path("./schemas")
# TODO: - Override `UsersStream` and `GroupsStream` with your own stream definition.
#       - Copy-paste as many times as needed to create multiple stream types.


class InspectionLotStream(SapStream):
    """Define custom stream."""
    name = "inspectionlot"
    path = "/A_InspectionLot?%24top=50"
    primary_keys = ["InspectionLot"]
    # Optionally, you may also use `schema_filepath` in place of `schema`:
    # schema_filepath = SCHEMAS_DIR / "users.json"
    records_jsonpath = "$.d.results[*]"  # Or override `parse_response`.
    schema = th.PropertiesList(
        th.Property("InspectionLot", th.StringType),
        th.Property("InspectionLotOrigin", th.StringType),
        th.Property("InspectionLotText", th.StringType),
        th.Property("InspectionLotText", th.StringType),
        th.Property("InspectionLotQuantity", th.StringType),
        th.Property("InspectionLotActualQuantity", th.StringType),
        th.Property("InspectionLotDefectiveQuantity", th.StringType),
        th.Property("InspLotCreatedOnLocalDate", th.StringType),
        th.Property("InspLotNmbrOpenLongTermCharc", th.IntegerType),
        th.Property("InspectionLotHasQuantity", th.BooleanType),
        th.Property("InspLotIsCreatedAutomatically", th.BooleanType),
        th.Property("InspectionLotHasPartialLots", th.BooleanType),
        th.Property("InspectionLotHasAppraisalCosts", th.BooleanType),
        th.Property("InspLotHasSubsets", th.BooleanType),
        th.Property("InspLotIsAutomUsgeDcsnPossible", th.BooleanType),
        th.Property("InspLotCreatedOnLocalTime", th.StringType),
        th.Property("InspectionLotCreatedBy", th.StringType),
        th.Property("InspectionLotCreatedOn", th.StringType),
        th.Property("InspectionLotCreatedOnTime", th.StringType),
        th.Property("InspectionLotChangedBy", th.StringType),
        th.Property("InspectionLotChangeDate", th.StringType),
        th.Property("InspectionLotChangeTime", th.StringType),
        th.Property("InspectionLotStartDate", th.StringType),
        th.Property("InspectionLotStartTime", th.StringType),
        th.Property("InspectionLotEndDate", th.StringType),
        th.Property("InspectionLotEndTime", th.StringType),
        th.Property("InspLotSelectionMaterial", th.StringType),
        th.Property("InspLotSelMatlRevisionLvl", th.StringType),
        th.Property("InspLotSelectionPlant", th.StringType),
        th.Property("InspLotSelectionSupplier", th.StringType),
        th.Property("InspLotSelectionManufacturer", th.StringType),
        th.Property("InspLotSelectionCustomer", th.StringType),
        th.Property("InspLotSelectionValidFromDate", th.StringType),
        th.Property("InspectionLotPlant", th.StringType),
        th.Property("InspectionLotStorageLocation", th.StringType),
        th.Property("InspLotNmbrAddlRecordedCharc", th.IntegerType),
        th.Property("InspLotNmbrOpenShortTermCharc", th.IntegerType),
        th.Property("InspectionLotSampleQuantity", th.StringType),
        th.Property("InspectionLotSampleUnit", th.StringType),
        th.Property("InspLotDynamicRule", th.StringType),
        th.Property("InspLotDynamicTrggrPoint", th.StringType),
        th.Property("InspectionDynamicStage", th.StringType),
        th.Property("InspectionSeverity", th.StringType),
        th.Property("InspLotQtyToFree", th.StringType),
        th.Property("InspLotQtyToScrap", th.StringType),
        th.Property("InspLotQtyToSample", th.StringType),
        th.Property("InspLotQtyToBlocked", th.StringType),
        th.Property("InspLotQtyToReserves", th.StringType),
        th.Property("InspLotQtyToAnotherMaterial", th.StringType),
        th.Property("InspLotBatchTransferredTo", th.StringType),
        th.Property("InspLotQtyReturnedToSupplier", th.StringType),
        th.Property("InspLotQtyToSpecialStock", th.StringType),
        th.Property("InspLotQtyToOtherStock", th.StringType),
        th.Property("InspLotQtyToBePosted", th.StringType),
        th.Property("InspLotSmplQtyForLongTermChar", th.StringType),
        th.Property("InspLotQtyInspected", th.StringType),
        th.Property("InspLotQtyDestroyed", th.StringType),
        th.Property("InspectionLotScrapRatio", th.StringType),
        th.Property("InspLotUsageDecisionTool", th.StringType),
        th.Property("InspLotAcctAssgmtKey", th.StringType),
        th.Property("InspLotCostCollectorSalesOrder", th.StringType),
        th.Property("InspLotCostCollectorSlsOrdItem", th.StringType),
        th.Property("InspLotCostCollectorWBSElement", th.StringType),
        th.Property("InspectionLotPriorityPoints", th.IntegerType)     
    ).to_dict()